import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TextInput, Alert } from 'react-native';
import React from 'react';

export default function App() {
  const [input, setInput] = React.useState('')
  console.log(input)
  return (
    <View style={styles.container}>
      <TextInput
       style={styles.input}
       value={input}
       placeholder="Type Anything"
       textAlign = "center"
       onChangeText={setInput} />
       <Button title="Submit"
       color= "black"
       onPress={ () => Alert.alert("Alert", "You just typed: " + input)} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#777',
    padding: 8,
    margin: 10,
    width: 200,
  }
});
