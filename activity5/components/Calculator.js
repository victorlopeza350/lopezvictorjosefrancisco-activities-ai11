import React, { Component } from 'react';
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import Button from './NumberPads';
import Screen from './Screen';

class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expression: '',
      result: '',
      actualExpression: '',
      isEqualPress: false,
    };
    
  }

  getButtonPressedVal = (buttonPressed) => {
    const { expression, actualExpression, isEqualPress, } = this.state;
    let newExpression;
    let newActalExp ;
    if (isEqualPress) {
      newExpression = `${buttonPressed}`;

    } else newExpression = `${expression}${buttonPressed}`;


    this.setState({
      expression: newExpression,
    });

    let actualChar = buttonPressed;
    if (actualChar === '÷') {
      actualChar = '/';
    } else if (actualChar === '×') {
      actualChar = '*';
    } else if (actualChar === '−') {
      actualChar = '-';
    }

    if(isEqualPress){newActalExp=`${actualChar}`}
    else{
    newActalExp = `${actualExpression}${actualChar}`;
    }
    
    this.setState({
      actualExpression: newActalExp,
      isEqualPress:false
    });

    try {
      this.setState({
        result: eval(newActalExp).toString(),
      });
    } catch (e) {
      console.log('error');
    }
  };

  allClear = () => {
    this.setState({
      expression: '',
      result: '',
      actualExpression: '',
    });
  };

  del = () => {
    const { expression, actualExpression, result, prevRes } = this.state;
    let newActalExp = actualExpression.substring(
      0,
      actualExpression.length - 1
    );
    this.setState({
      expression: expression.substring(0, expression.length - 1),
      actualExpression: newActalExp,
    });
    try {
      this.setState({
        result: eval(newActalExp).toString(),
      });
    } catch (e) {
      try {
        this.setState({
          result: eval(
            newActalExp.substring(0, expression.length - 2)
          ).toString(),
        });
      } catch (e) {
        this.setState({ expression: '', result: '', actualExpression: '' });
      }
    }
  };

  onEqualPress = () => {
    const { expression, result, isEqualPress } = this.state;
    const hist = { expression, result };
    this.setState({
      expression: result,
      result: '',
      isEqualPress: true,
    });
  };


  render() {
    const {
      expression,
      result,
      actualExpression,
    } = this.state;

    return (
      <>
        <View style={styles.container}>
          <StatusBar barStyle="light-content" backgroundColor="darkred" />

          <Screen
            style={styles.screen}
            expression={expression}
            result={result}
          />
          <Button
            style={styles.button}
            getButtonPressedVal={this.getButtonPressedVal}
            allClear={this.allClear}
            del={this.del}
            onEqualPress={this.onEqualPress}
          />
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
  },
});

export default Calculator;