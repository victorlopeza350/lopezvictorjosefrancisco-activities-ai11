import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Screens
import HomeScreen from './screens/HomeScreen';
import CompletedTaskScreen from './screens/CompletedTaskScreen';

//Screen names
const homeName = "Home";
const completedName = "Completed Tasks";

const Tab = createBottomTabNavigator();

function MainContainer() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName={homeName}
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName
            let routeName = route.name

            if (routeName === homeName) {
              iconName = focused ? 'home' : 'home-outline'
            }
            else if (routeName === completedName) {
              iconName = focused ? 'list' : 'list-outline'
            }
            return <Ionicons name={iconName} size={size} color={color}/>
          },
          tabBarActiveTintColor: 'orange',
          tabBarInactiveTintColor: 'grey',
          tabBarLabelStyle: { paddingBottom:10, fontSize:10},
          tabBarStyle: {padding: 10, height: 60}
        })}>

        <Tab.Screen name={homeName} component={HomeScreen} />
        <Tab.Screen name={completedName} component={CompletedTaskScreen} />

      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default MainContainer;