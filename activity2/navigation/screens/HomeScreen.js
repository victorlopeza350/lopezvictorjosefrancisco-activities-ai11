import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
export default function App() {
  const [getText, setText] = useState('');
  const [getList, setList] = useState([]);
  
  const addItem = () => {
    console.log(getText);
    setList([
        
      {key:Math.random().toString() 
        , data:getText},
        ...getList
        
      ]);
      
    setText('');
   
  }
  const removeItem = (itemKey) => {
    setList(list => getList.filter(item => item.key != itemKey));
  }
  return (
    <View style={styles.container}>
      <Text style={styles.title}></Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.textInput}
          placeholder="Add a Task"
          onChangeText={text => setText(text)}
          value={getText}
        />
        <Button
       
          title="Add Task"
          onPress={addItem}
          disabled={getText.length <= 0}
/>
       

      </View>
      <ScrollView style={styles.scrollview}>
        {getList.map((item) => 
        <TouchableOpacity
          key={item.key}
          activeOpacity={0.5}
         
        >
        <View style={styles.scrollviewItem} >
          <Text style={styles.scrollviewText}>{item.data}</Text>
          <TouchableOpacity
           onPress={()=>removeItem(item.key)}>
          <View style={styles.checktextcontainer}>
            <Text style={styles.checkmark}>✓</Text>
          </View>
          </TouchableOpacity>
        </View>
        </TouchableOpacity>
        )}
      </ScrollView>
      
    </View>
  );
}

const styles = StyleSheet.create({
  
  checktextcontainer: {
    backgroundColor: 'green',
    borderRadius: 10,
    padding: 5,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkmark: {
    fontSize: 20,
    color: 'white',
    backgroundColor: 'green',
    
  },
  scrollviewText: {
    fontSize: 24,
    color: 'black',
    flex: 1,
  },
  scrollview: {
    paddingTop: 20,
    width: '100%',
  },
  scrollviewItem: {
    margin: 7,
    padding: 20,
    backgroundColor: '#f2f2f2',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'gray',
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: 64,
    color: 'lightgrey',
  },
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 40,
  },
  inputContainer: {
    flexDirection: "column",
    width: '70%',
    justifyContent: "space-between",
    alignItems: "center",
  },
  textInput: {
    padding: 12,
    borderColor: '#000000',
    width: '100%',
    marginBottom: 10,
    borderWidth: 1,
    borderRadius: 10,
    textAlign: 'center',
  }
});