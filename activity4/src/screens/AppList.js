import React from 'react';
import {SafeAreaView, Image, FlatList, View, Text, StatusBar, TouchableOpacity, Dimensions,} from 'react-native';
import styles from './AppList.styles';

const {width, height} = Dimensions.get('window');
const colors = {primary: '#fff', white: '#fff'};
const slides = [
  {
    id: '1',
    image: require('../images/screen1.png'),
    title: 'Introduction',
    subtitle: 'Heyy, My name is Victor and here are a few reasons on why I chose the IT course.',
  },
  {
    id: '2',
    image: require('../images/screen2.png'),
    title: 'A Dream',
    subtitle: 'Since I was young I was always fascinated and curious about technology',
  },
  {
    id: '3',
    image: require('../images/screen3.png'),
    title: 'Technology is Evolving',
    subtitle: 'As the world evolves so does technology, that is why we need more people who work in this industry to prepare ourselves for the future.',
  },
  {
    id: '4',
    image: require('../images/screen4.png'),
    title: 'To Gain and Share Ideas',
    subtitle: 'We all know these days that it is hard to do something by our own especially in the field and that is why I chose IT because I want to share the knowledge a gained and also learn from experienced people.',
  },
  {
    id: '5',
    image: require('../images/screen5.png'),
    title: 'A lot of Opportunities',
    subtitle: 'Being in the IT industry give us a huge advantage and that is a lot of opportunities because a lot of jobs now are automated so with that companies would need IT people.',
  },
  {
    id: '6',
    image: require('../images/screen6.png'),
    title: 'Meet New People',
    subtitle: 'In the IT industry we would get a chance to meet a lot of people along the way may this be high ranking people or your new co-workers',
  },
];

const Slide = ({item}) => {
  return (
    <View style={{alignItems: 'center'}}>
      <Image
        source={item?.image}
        style={{height: '75%', width, resizeMode: 'contain'}}
      />
      <View>
        <Text style={styles.title}>{item?.title}</Text>
        <Text style={styles.subtitle}>{item?.subtitle}</Text>
      </View>
    </View>
  );
};

const AppList = ({navigation}) => {
  const [currentSlideIndex, setCurrentSlideIndex] = React.useState(0);
  const ref = React.useRef();
  const updateCurrentSlideIndex = e => {
    const contentOffsetX = e.nativeEvent.contentOffset.x;
    const currentIndex = Math.round(contentOffsetX / width);
    setCurrentSlideIndex(currentIndex);
  };

  const goToNextSlide = () => {
    const nextSlideIndex = currentSlideIndex + 1;
    if (nextSlideIndex != slides.length) {
      const offset = nextSlideIndex * width;
      ref?.current.scrollToOffset({offset});
      setCurrentSlideIndex(currentSlideIndex + 1);
    }
  };

  const skip = () => {
    const lastSlideIndex = slides.length - 1;
    const offset = lastSlideIndex * width;
    ref?.current.scrollToOffset({offset});
    setCurrentSlideIndex(lastSlideIndex);
  };

  const Footer = () => {
    return (
      <View
        style={{
          height: height * 0.2,
          justifyContent: 'space-between',
          paddingHorizontal: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 20,
          }}>
          {slides.map((_, index) => (
            <View
              key={index}
              style={[
                styles.indicator,
                currentSlideIndex == index && {
                  backgroundColor: colors.white,
                  width: 25,
                },
              ]}
            />
          ))}
        </View>
        <View style={{marginBottom: 20}}>
          {currentSlideIndex == slides.length - 1 ? (
            <View style={{height: 50}}>
              <TouchableOpacity
                style={styles.btn}
                onPress={() => navigation.replace('App List')}>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  HOME
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{flexDirection: 'row'}}>
             
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={goToNextSlide}
                style={styles.btn}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}>
                  NEXT
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.primary}}>
      <StatusBar backgroundColor={colors.primary} />
      <FlatList 
        ref={ref}
        onMomentumScrollEnd={updateCurrentSlideIndex}
        contentContainerStyle={{height: height * 0.75}}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={slides}
        pagingEnabled
        renderItem={({item}) => <Slide item={item} />}
      />
      <Footer />
    </SafeAreaView>
  );
};
export default AppList;