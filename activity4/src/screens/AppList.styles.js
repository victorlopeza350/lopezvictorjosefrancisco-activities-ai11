import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
    subtitle: {
      color: 'black',
      fontSize: 13,
      marginBottom: 1,
      maxWidth: width * 0.9,
      textAlign: 'center',
      lineHeight: 23,
    },
    title: {
      color: 'black',
      fontSize: 22,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    image: {
      height: '100%',
      width: '100%',
      resizeMode: 'contain',
    },
    indicator: {
      height: 2.5,
      width: 10,
      backgroundColor: '#6C63FF',
      marginHorizontal: 3,
      borderRadius: 2,
    },
    btn: {
      flex: 1,
      height: 50,
      borderRadius: 20,
      backgroundColor: '#6C63FF',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });