import React from "react";
import { StyleSheet, Text, View, ScrollView, ImageBackground } from "react-native";


export default function App() {
    return (

        <View style={styles.container}>
            <ImageBackground style={styles.bg}
                resizeMode='cover'
                source={require('./bg.png')}>
                    <ScrollView>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 5, marginLeft: 5, marginRight: 5,}}>
                            INGREDIENTS
                        </Text>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', marginLeft: 5, marginRight: 5, padding: 5, lineHeight: 25,}}>
                        •125 g Chang’s Pad Thai dried rice sticks{"\n"}
                        •1 1/2 Tbsp tamarind puree{"\n"}
                        •3 Tbsp brown sugar{"\n"}
                        •2 Tbsp fish sauce{"\n"}
                        •1 1/2 Tbsp oyster sauce{"\n"}
                        •2 – 3 Tbsp vegetable or canola oil{"\n"}
                        •1/2 Onion{"\n"}
                        •2 Garlic cloves{"\n"}
                        •150 g Chicken thigh{"\n"}
                        •2 Eggs{"\n"}
                        •1 1/2 Cups of beansprouts{"\n"}
                        •1/4 Cup garlic chives{"\n"}
                        •1/4 Cup finely chopped peanuts
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 50, marginLeft: 5, marginRight: 5,}}>
                            PROCEDURE
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                            STEP 1
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Place noodles in a large bowl, pour over plenty of boiling water. Soak for 5 minutes, then drain in a colander and quickly rinse under cold water. Don’t leave them sitting around for more than 5 – 10 minutes.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 2
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Mix Sauce in small bowl.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 3
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Heat 2 tbsp oil in a large non stick pan (or well seasoned skillet) over high heat. Add garlic and onion, cook for 30 seconds.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 4
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add chicken and cook for 1 1/2 minutes until mostly cooked through.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 5
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Push to one side of the pan, pour egg in on the other side. Scramble using the wooden spoon (add touch of extra oil if pan is too dry), then mix into chicken.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 6
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add bean sprouts, tofu, noodles then Sauce.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 7
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Toss gently for about 1 1/2 minutes until Sauce is absorbed by the noodles.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 8
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add garlic chives and half the peanuts. Toss through quickly then remove from heat.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 9
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Serve immediately, sprinkled with remaining peanuts and lime wedges on the side, with a sprinkle of chilli and a handful of extra beansprouts on the side if desired (this is the Thai way!). Squeeze over lime juice to taste before eating.
                        </Text>
                        
                    </ScrollView>
                    

            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bg:{
        width: '100%',
        height: '100%',
        flex: 1,
    },
    
});