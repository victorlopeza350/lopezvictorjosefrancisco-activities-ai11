import React from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity, Image } from "react-native";


export default function Boxes({ navigation }) {
    return (
        <ScrollView>
        <View style={styles.container}>

        <View style={styles.box}>
            <TouchableOpacity style={styles.inner}  onPress = {() => navigation.navigate('Chicken Teriyaki')}>
                <Image source={require('./1.png' )} />
            </TouchableOpacity>
        </View>

        <View style={styles.box}>
            <TouchableOpacity style={styles.inner}  onPress = {() => navigation.navigate('Beef Wellington')}>
                <Image source={require('./2.png' )} />
            </TouchableOpacity>
        </View>

        <View style={styles.box}>
            <TouchableOpacity style={styles.inner}  onPress = {() => navigation.navigate('Pork Stew')}>
                <Image source={require('./3.png' )} />
            </TouchableOpacity>
        </View>

        <View style={styles.box}>
            <TouchableOpacity style={styles.inner}  onPress = {() => navigation.navigate('Pad Thai')}>
                <Image source={require('./4.png' )} />
            </TouchableOpacity>
        </View>

        <View style={styles.box}>
            <TouchableOpacity style={styles.inner}  onPress = {() => navigation.navigate('Carbonara')}>
                <Image source={require('./5.png' )} />
            </TouchableOpacity>
        </View>

        <View style={styles.box}>
            <TouchableOpacity style={styles.inner}  onPress = {() => navigation.navigate('Fried Chicken')}>
                <Image source={require('./6.png' )} />
            </TouchableOpacity>
        </View>

    </View>
    </ScrollView>
);
}

const styles = StyleSheet.create({
container: {
width: '100%',
height: '60%',
padding: 5,
flexDirection: 'row',
flexWrap: 'wrap',

},
box: {
width: '50%',
height: '50%',
padding: 5

},

inner: {
flex: 1,
backgroundColor: '#eee',
},

});