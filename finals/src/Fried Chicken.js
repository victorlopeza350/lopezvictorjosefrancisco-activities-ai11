import React from "react";
import { StyleSheet, Text, View, ScrollView, ImageBackground } from "react-native";


export default function App() {
    return (

        <View style={styles.container}>
            <ImageBackground style={styles.bg}
                resizeMode='cover'
                source={require('./bg.png')}>
                    <ScrollView>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 5, marginLeft: 5, marginRight: 5,}}>
                            INGREDIENTS
                        </Text>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', marginLeft: 5, marginRight: 5, padding: 5, lineHeight: 25,}}>
                        •2 Whole Chickens{"\n"}
                        •Vegetable oil{"\n"}
                        •6 Cups all-purpose flour{"\n"}
                        •5 Tbsp salt{"\n"}
                        •4 Tbsp ground black pepper{"\n"}
                        •2 Tbsp garlic powder{"\n"}
                        •1 Tbsp onion powder{"\n"}
                        •2 Tspn cayenne pepper{"\n"}
                        •2 Cups buttermilk
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 50, marginLeft: 5, marginRight: 5,}}>
                            PROCEDURE
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                            STEP 1
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Cut the whole chickens into 4 breasts, 4 thighs, 4 legs and 4 wings and set aside.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 2
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Preheat your oil, in either a heavy pan on the stove or a deep-fryer, to 325 degrees F.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 3
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        In a large bowl, combine the flour, salt, black pepper, garlic powder, onion powder and cayenne pepper until thoroughly mixed. Set aside.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 4
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Pour the buttermilk into another bowl large enough for the chicken to be immersed in the buttermilk.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 5
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Prepare your dredging station. Place your chicken in a bowl. Next to that, your bowl of buttermilk, and next to that, your dry mixture.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 6
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Take your breasts, lightly dust them with your flour mixture, then dip them in the buttermilk until they are coated, and then place them in the flour mixture.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 7
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Take the breasts that are in the flour mixture and aggressively push the flour mixture into the wet chicken. Make sure that the chicken in very thoroughly coated, or you will not achieve the crust and crunch you are looking for. Gently place the breasts in your hot oil.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 8
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Next, repeat the dredging steps with your other pieces of chicken in this order: thigh, leg then wing.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 9
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        When you place the last wing into the fryer, you should have 16 pieces of chicken in the oil. Set a timer for 15 minutes.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 10
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        After 15 minutes, take a probe thermometer and check the temperature of a breast. If it reads 180 degrees F, all of your chicken is done. (Keep in mind that it will continue to cook after it has been removed from the fryer.)
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 11
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Remove the chicken from the oil and let it drain for 5 minutes. Let cool for an additional 10 minutes before serving.
                        </Text>
                        
                    </ScrollView>
                    

            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bg:{
        width: '100%',
        height: '100%',
        flex: 1,
    },
    
});