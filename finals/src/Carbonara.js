import React from "react";
import { StyleSheet, Text, View, ScrollView, ImageBackground } from "react-native";


export default function App() {
    return (

        <View style={styles.container}>
            <ImageBackground style={styles.bg}
                resizeMode='cover'
                source={require('./bg.png')}>
                    <ScrollView>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 5, marginLeft: 5, marginRight: 5,}}>
                            INGREDIENTS
                        </Text>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', marginLeft: 5, marginRight: 5, padding: 5, lineHeight: 25,}}>
                        •3/4 lb. Spaghetti{"\n"}
                        •1 Beef cube{"\n"}
                        •7 Strips bacon{"\n"}
                        •15 Oz. all-purpose cream{"\n"}
                        •6 Tbsp grated Parmesan cheese{"\n"}
                        •1 Yellow onion{"\n"}
                        •2 Cloves garlic{"\n"}
                        •1/8 Tspn ground nutmege{"\n"}
                        •1 Tspn salt{"\n"}
                        •2 Qt. water
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 50, marginLeft: 5, marginRight: 5,}}>
                            PROCEDURE
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                            STEP 1
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Cook the bacon until crispy. Do this by placing each strip in a pan. Cook in medium heat for 2 minutes. Flip the bacon and cook the opposite side for another 2 minutes. Continue performing the same steps until each piece gets crispy. Set aside. Save the bacon grease.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 2
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Cook the spaghetti by pouring water in a cooking pot. Let boil. Add salt and put the spaghetti into the pot. Cook for 12 minutes or until al dente. Remove the spaghetti Set aside. Save ¼ cup of water used to cook spaghetti.

                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 3
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Prepare the carbonara sauce by heating 3 tablespoons of bacon grease. Saute onion and garlic.

                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 4
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        One the onion softens, pour all-purpose cream into the pan. Stir. Add ¼ cup water
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 5
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add the Beef Cube. Stir until well blended
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 6
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Put chopped bacon and nutmeg into the sauce. Stir.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 7
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add the cooked spaghetti. Toss until spaghetti gets totally coated with sauce.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 8
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add Parmesan cheese. Toss. Add more bacon.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 9
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Transfer to a serving plate. Serve and enjoy!
                        </Text>
                        
                    </ScrollView>
                    

            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bg:{
        width: '100%',
        height: '100%',
        flex: 1,
    },
    
});