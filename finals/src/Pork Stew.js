import React from "react";
import { StyleSheet, Text, View, ScrollView, ImageBackground } from "react-native";


export default function App() {
    return (

        <View style={styles.container}>
            <ImageBackground style={styles.bg}
                resizeMode='cover'
                source={require('./bg.png')}>
                    <ScrollView>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 5, marginLeft: 5, marginRight: 5,}}>
                            INGREDIENTS
                        </Text>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', marginLeft: 5, marginRight: 5, padding: 5, lineHeight: 25,}}>
                        •2 (1/2 lb.) Pork shoulder{"\n"}
                        •1/2 Cup all-purpose flour{"\n"}
                        •Salt{"\n"}
                        •Black pepper{"\n"}
                        •2-4 Tbsp vegetable oil{"\n"}
                        •4 Slices bacon{"\n"}
                        •1 Large onion{"\n"}
                        •1 Tbsp garlic{"\n"}
                        •2 Cups carrots{"\n"}
                        •1 Cup celery{"\n"}
                        •1/2 Cup white wine{"\n"}
                        •4 Cups beef broth{"\n"}
                        •2 Tbsp tomato paste{"\n"}
                        •1 Tspn dried thyme leaves{"\n"}
                        •1/2 Tspn dried rosemary leaves{"\n"}
                        •2 Bay leaves{"\n"}
                        •1/2 Cup pitted prunes{"\n"}
                        •2 Russet potatoes{"\n"}
                        •1 Cup parsnips{"\n"}
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 50, marginLeft: 5, marginRight: 5,}}>
                            PROCEDURE
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                            STEP 1
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        In a mixing bowl, toss the pork with the flour, 2 teaspoons salt, and 1 teaspoon black pepper to coat evenly, shaking off excess.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 2
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Heat 2 tablespoons of oil, over medium-high heat, in a large pot and brown pork in batches without crowding it, about 7-9 minutes. Add more oil as needed. Transfer browned meat to a bowl with a slotted spoon and set aside. Remove and discard pan drippings.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 3
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Cook the bacon in the same pot, stirring frequently until crispy. Transfer to bowl with pork.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 4
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Reduce heat to medium and saute onion, garlic, carrots, and celery, in bacon drippings, stirring occasionally, until soft.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 5
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add wine to the pot, continue to simmer, scraping up brown bits on the bottom of the pot until liquid is almost evaporated.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 6
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Add beef broth, tomato paste, browned pork, bacon, thyme, rosemary, bay leaves, 1 teaspoon salt, and 1/2 teaspoon black pepper. Bring to a boil, then reduce heat to low and simmer, covered for 45 minutes.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 7
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Stir in prunes, potatoes and parsnips then cover and cook for 45 minutes, stirring occasionally to prevent sticking. If you'd like the stew thinner, add more beef broth or water to achieve desired consistency. Adjust salt and pepper as needed. Ladle into individual bowls, garnish with parsley and serve.
                        </Text>
                        
                    </ScrollView>
                    

            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bg:{
        width: '100%',
        height: '100%',
        flex: 1,
    },
    
});