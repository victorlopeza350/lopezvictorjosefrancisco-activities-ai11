import React from "react";
import { StyleSheet, Text, View, ScrollView, ImageBackground } from "react-native";


export default function App() {
    return (

        <View style={styles.container}>
            <ImageBackground style={styles.bg}
                resizeMode='cover'
                source={require('./bg.png')}>
                    <ScrollView>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 5, marginLeft: 5, marginRight: 5,}}>
                            INGREDIENTS
                        </Text>
                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', marginLeft: 5, marginRight: 5, padding: 5, lineHeight: 25,}}>
                        •1 Cup soy sauce{"\n"}
                        •1 Cup granulated sugar{"\n"}
                        •1 ½ Tspn brown sugar{"\n"}
                        •6 Cloves garlic, crushed in a press{"\n"}
                        •2 Tbsp grated fresh ginger{"\n"}
                        •¼ Tspn freshly ground black pepper{"\n"}
                        •1 3-Inch cinnamon stick{"\n"}
                        •1 Tbsp pineapple juice{"\n"}
                        •8 Skinless, boneless chicken thighs{"\n"}
                        •2 Tbsp cornstarch
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'center', letterSpacing: 5, padding: 50, marginLeft: 5, marginRight: 5,}}>
                            PROCEDURE
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                            STEP 1
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        In a small saucepan, combine all ingredients except cornstarch and chicken. Bring to boil over high heat. Reduce heat to low and stir until sugar is dissolved, about 3 minutes. Remove from heat and let cool. Discard cinnamon stick and mix in 1/2 cup water.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 2
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Place chicken in a heavy-duty sealable plastic bag. Add soy sauce mixture, seal bag, and turn to coat chicken. Refrigerate for at least an hour, ideally overnight.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 3
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Remove chicken and set aside. Pour mixture into a small saucepan. Bring to a boil over high heat, then reduce heat to low. Mix cornstarch with 2 tablespoons water and add to pan. Stir until mixture begins to thicken, and gradually stir in enough water (about 1/2 cup) until sauce is the consistency of heavy cream. Remove from heat and set aside.
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'left', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        {"\n"} STEP 4
                        </Text>

                        <Text style={{color: '#fff', fontSize: 17, textAlign: 'justify', padding: 5, marginLeft: 5, marginRight: 5,}}>
                        Preheat a broiler or grill. Lightly brush chicken pieces on all sides with sauce, and broil or grill about 3 minutes per side. While chicken is cooking, place sauce over high heat and bring to a boil, then reduce heat to a bare simmer, adding water a bit at a time to keep mixture at a pourable consistency. To serve, slice chicken into strips, arrange on plates, and drizzle with sauce.
                        </Text>
                        
                    </ScrollView>
                    

            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bg:{
        width: '100%',
        height: '100%',
        flex: 1,
    },
    
});