import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Boxes from "./src/Boxes"
import First from "./src/Chicken Teriyaki"
import Second from "./src/Beef Wellington"
import Third from "./src/Pork Stew"
import Fourth from "./src/Pad Thai"
import Fifth from "./src/Carbonara"
import Sixth from "./src/Fried Chicken"


const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen
        name="Food Recipes"
        component={Boxes}
        options={{
          title: 'Food Recipes',
          headerStyle: {
            backgroundColor: '#fff',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 25
          },
        }}
      />
        <Stack.Screen
          name='Chicken Teriyaki'
          component={First}
        />
         <Stack.Screen
          name='Beef Wellington'
          component={Second}
        />
          <Stack.Screen
          name='Pork Stew'
          component={Third}
        />
          <Stack.Screen
          name='Pad Thai'
          component={Fourth}
        />
          <Stack.Screen
          name='Carbonara'
          component={Fifth}
        />
          <Stack.Screen
          name='Fried Chicken'
          component={Sixth}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
